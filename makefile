csrc = $(wildcard *.c)
obj = $(csrc:.c=.o)

CC = gcc

CFLAGS  = -Wall -O2
LDFLAGS = -lm -lpulse -lpulse-simple -lfftw3

obj/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

cpulse: obj/$(obj)
	$(CC) -o $@ $^ $(LDFLAGS) $(CFLAGS)

clean:
	rm -rf ./obj/*
	rm -rf cpulse
	rm -rf *.o