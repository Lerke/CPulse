#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pulse/simple.h>
#include <pulse/error.h>
#include <fftw3.h>
#include <math.h>
#include <pulse/sample.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <termios.h> 

#define SAMPLERATE (44100)
#define BUFSIZE (1024)

#define SEGMENTS 8
#define YSCALE 8

#define SCALE_MAX_DB 100

#define FREQ_PER_SEGMENT ((MAX_FREQ) / (SEGMENTS))
#define SAMPLES_PER_SEGMENT ((int)((BUFSIZE/2) / (SEGMENTS)))

#define OUTPUT_PORT "/dev/ttyUSB0"

typedef enum {
  CPULSE_ERR_NONE         = 0,
  CPULSE_ERR_SERIAL       = 1,
  CPULSE_ERR_INVALID_ARGS = 2,
  CPULSE_ERR_PULSEAUDIO_F = 3,
} CPULSE_ERRNO;

// PCM Audio Sample from PulseAudio
// Assumed 16 bit LE Signed PCM
typedef union {
  int16_t s;                                // 16 Bit Sample. Audio sample if 1 channel
  int8_t  lr[2];                            // Left/Right interleaved audio when 2 channel
} audio_sample_t;

// Output Calculated Sample
typedef struct
{
  double   amplitude;                       // Amplitude (in Db). (0-130)
  uint16_t frequency;                       // Average frequency of the samples
} output_sample_t;

// The output is defined as (SEGMENTS) ranges symbolizing a bounded region in frequency space.
typedef struct {
  struct output_segment_scale_range_t {
    uint16_t begin;                         // Begin (in Hz) of this band. Inclusive
    uint16_t end;                           // End   (in Hz) of this band. Exclusive
    uint16_t samples;                       // Number of samples in this band
  } ranges[SEGMENTS];                       // Set of spectrum bands [begin,end)
  uint16_t lo_pass;                         // Low  Pass (in Hz)
  uint16_t hi_pass;                         // High Pass (in Hz)
} output_segment_scale_t;

// The CPulse Generator structure combining all parts required to output a spectrum.
typedef struct {
  // PulseAudio support
  pa_simple*     pulseaudio_strm;           // Simple API
  pa_sample_spec sample_spec;               // PulseAudio Sample Specification

  // FFTW stuff
  fftw_plan     fftplan;                    // FFTW Plan
  double*       input_buffer;               // Real   input buffer   (PCM -> FFT       )
  fftw_complex* output_buffer;              // Complex output buffer (FFT -> (freq,mag))

  // Member data
  audio_sample_t          input[BUFSIZE];   // PCM Input
  uint64_t                last_output;      // Last output frame, don't retransmit identical frames
  output_sample_t         output[SEGMENTS]; // output samples
  output_segment_scale_t  scale;            // Output scale
  
  // Buffers & Descriptors
  char output_bfr[50];                      // Character output buffer (to serial port)
  int  serial_out_fd;                       // Serial output file descriptor
} cpulse_generator_t;

static cpulse_generator_t* cpulse_generator_init(char* output_port)
{
  // Create space for ourself!
  cpulse_generator_t* self = malloc(sizeof(cpulse_generator_t));

  // Define PulseAudio sample specification
  self->sample_spec.format   = PA_SAMPLE_S16LE;
  self->sample_spec.rate     = SAMPLERATE;
  self->sample_spec.channels = 1;

  // Init PulseAudio API
  // todo: configure buffer?
  self->pulseaudio_strm = pa_simple_new(NULL, "CPulse", PA_STREAM_RECORD, NULL, "record",
                                        &self->sample_spec, NULL, NULL, NULL);

  // Define output scale
  // Example output scale.
  //        [     Hz    -     Hz      ]
  // Band 1 [     0     -     40      ]
  // Band 2 [     40    -     160     ]
  // Band 3 [     160   -     300     ]
  // Band 4 [     300   -     800     ]
  // Band 5 [     800   -     2500    ]
  // Band 6 [     2500  -     5000    ]
  // Band 7 [     5000  -     10000   ]
  // Band 8 [     10000 -     20000   ]
  // No Low or High Pass
  self->scale.ranges[0].begin = 0    ; self->scale.ranges[0].end = 40   ; self->scale.ranges[0].samples = 0;
  self->scale.ranges[1].begin = 40   ; self->scale.ranges[1].end = 160  ; self->scale.ranges[1].samples = 0;
  self->scale.ranges[2].begin = 160  ; self->scale.ranges[2].end = 300  ; self->scale.ranges[2].samples = 0;
  self->scale.ranges[3].begin = 300  ; self->scale.ranges[3].end = 800  ; self->scale.ranges[3].samples = 0;
  self->scale.ranges[4].begin = 800  ; self->scale.ranges[4].end = 2500 ; self->scale.ranges[4].samples = 0;
  self->scale.ranges[5].begin = 2500 ; self->scale.ranges[5].end = 5000 ; self->scale.ranges[5].samples = 0;
  self->scale.ranges[6].begin = 5000 ; self->scale.ranges[6].end = 10000; self->scale.ranges[6].samples = 0;
  self->scale.ranges[7].begin = 10000; self->scale.ranges[7].end = 20000; self->scale.ranges[7].samples = 0;
  self->scale.lo_pass = 0;
  self->scale.hi_pass = 0;

  // Setup Buffers
  self->input_buffer  = fftw_malloc(BUFSIZE           * sizeof(double      ));
  self->output_buffer = fftw_malloc(((BUFSIZE/2) + 1) * sizeof(fftw_complex));

  // Setup FFT
  self->fftplan = fftw_plan_dft_r2c_1d(BUFSIZE, self->input_buffer, self->output_buffer, FFTW_MEASURE);

  // Setup Serial Port
  self->serial_out_fd = open(output_port, O_WRONLY | O_NOCTTY | O_NONBLOCK);

  if(self->serial_out_fd == -1) 
  {
    printf("Could not open Serial portr\r\n");
    exit(CPULSE_ERR_SERIAL);
  }

  return self;
}

void cpulse_generator_write_frame(cpulse_generator_t* self)
{ 
  //  Output frame (8 x 8 = 64)
  uint64_t output = self->output[0].amplitude + self->output[1].amplitude + self->output[2].amplitude +
  self->output[3].amplitude + self->output[4].amplitude + self->output[5].amplitude + self->output[6].amplitude +
  self->output[7].amplitude;

  // Don't retransmit the same image!
  if(self->last_output != output)
  {
    // Write formatted output to serial port.
    write(self->serial_out_fd,
      self->output_bfr,
      (sprintf(
        self->output_bfr,
        "{\"samples\":[%1d,%1d,%1d,%1d,%1d,%1d,%1d,%1d]}\r",
        (uint8_t) self->output[0].amplitude,
        (uint8_t) self->output[1].amplitude,
        (uint8_t) self->output[2].amplitude,
        (uint8_t) self->output[3].amplitude,
        (uint8_t) self->output[4].amplitude,
        (uint8_t) self->output[5].amplitude,
        (uint8_t) self->output[6].amplitude,
        (uint8_t) self->output[7].amplitude)
       )-1);
       self->last_output = output;
  }
}

void cpulse_generator_generate_frame(cpulse_generator_t* self)
{
  // Apply Hanning window on raw PCM input.
  for (int i = 0; i < BUFSIZE; i++)
  {
    self->input[i].s *= 0.5 * (1.0 - cos(2 * M_PI * i / (BUFSIZE - 1)));
  }

  // Stack input buffer with data
  for (int i = 0; i < BUFSIZE; i++)
  {
    self->input_buffer[i] = (double)(self->input[i].s);
  }
  // Execute FFT
  fftw_execute(self->fftplan);

  // Lets collect FFT output
  memset(self->output, 0, sizeof(self->output));

  for(int i =0; i < (BUFSIZE/2)-1 ; i++)
  {
    uint16_t frequency = i * (SAMPLERATE / BUFSIZE);
    double magnitude   = (pow((self->output_buffer[i][0]), 2) + pow((self->output_buffer[i][1]), 2));

    for(int s = 0; s < SEGMENTS; s++)
    {
      if((self->scale.ranges[s].begin <= frequency ) && (frequency < self->scale.ranges[s].end))
      {
        self->output[s].amplitude               += magnitude;
        self->output[s].frequency               += frequency;
        self->scale.ranges[s].samples += 1;
        break;
      }
    }
  }

  for(int s = 0; s < SEGMENTS; s++)
  {
    self->output[s].amplitude  = abs(((self->output[s].amplitude / self->scale.ranges[s].samples) > 0) 
                        ? 10 * log10((self->output[s].amplitude / self->scale.ranges[s].samples)) 
                        : 0);
    self->output[s].frequency /= (self->scale.ranges[s].samples == 0) ? 1 : self->scale.ranges[s].samples;
  }
  
  for(int i = 0; i < SEGMENTS; i++)
  {
    double f = round((1.0/SCALE_MAX_DB) * (self->output[i].amplitude) * YSCALE);
    self->output[i].amplitude  = f;
  }
}

static void cpulse_generator_loop(cpulse_generator_t* self)
{
  memset(self->input, 0, sizeof(self->input));

  for (;;)
  {
    /* Record some data ... */
    if (pa_simple_read(self->pulseaudio_strm, self->input, sizeof(self->input), NULL) == 0)
    {
      cpulse_generator_generate_frame(self);
      cpulse_generator_write_frame(self);
    }
    else
    {
      fprintf(stderr, __FILE__ ": pa_simple_read() failed.\r\n");;
      exit(CPULSE_ERR_PULSEAUDIO_F);
    }
  }
}

static void cpulse_generator_cleanup(cpulse_generator_t* self)
{
  // Clean up resources
  // PulseAudio
  if(self->pulseaudio_strm)
  {
    pa_simple_free(self->pulseaudio_strm);
  }

  // Serial Port
  close(self->serial_out_fd);

  // FFTW
  fftw_destroy_plan(self->fftplan);
  fftw_free(self->input_buffer);
  fftw_free(self->output_buffer);
}


// Clean this shit up
int main(int argc, char *argv[])
{
  if(argc < 2)
  {
    printf("Invalid number of arguments\r\n");
    printf("Usage: ./cpulse <com-port>\r\n");
    return CPULSE_ERR_INVALID_ARGS;
  }

  printf("\r\n"\
"   $$$$$$\\  $$$$$$$\\            $$\\                        \r\n  \
$$  __$$\\ $$  __$$\\           $$ |                        \r\n  \
$$ /  \\__|$$ |  $$ |$$\\   $$\\ $$ | $$$$$$$\\  $$$$$$\\   \r\n  \
$$ |      $$$$$$$  |$$ |  $$ |$$ |$$  _____|$$  __$$\\      \r\n  \
$$ |      $$  ____/ $$ |  $$ |$$ |\\$$$$$$\\  $$$$$$$$ |    \r\n  \
$$ |  $$\\ $$ |      $$ |  $$ |$$ | \\____$$\\ $$   ____|   \r\n  \
\\$$$$$$  |$$ |      \\$$$$$$  |$$ |$$$$$$$  |\\$$$$$$$\\   \r\n  \
 \\______/ \\__|       \\______/ \\__|\\_______/  \\_______|\r\n" \
  );

  cpulse_generator_t* cpulse = cpulse_generator_init(argv[1]);

  cpulse_generator_loop(cpulse);

  cpulse_generator_cleanup(cpulse);

  return CPULSE_ERR_NONE;
}